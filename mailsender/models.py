# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.db import transaction
from .tasks import create_message_for_new_commit_order, send_email


# Create your models here.

class Order(models.Model):
    client = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, null=True, blank=True)
    submitted = models.BooleanField(default=False, null=False, blank=False)
    last_modified = models.DateTimeField(default=None, blank=True, null=True)
    objects = models.Manager()

    @staticmethod
    def commit_order(client):
        with transaction.atomic():
            order = Order.objects.select_for_update().filter(client=client, submitted=False).first()
            if order and client == order.client:
                order.submitted = True
                order.last_modified = timezone.now()
                order.save()
            else:
                return

        create_message_for_new_commit_order(order.id)

    def __str__(self):
        return str(self.id) + ' ' + str(self.submitted) + ' Client: ' + str(self.client.username)

    def clean(self):
        # Проверяем, есть ли упользователя открытый заказ при попытке создать новый
        if not self.client:
            return
        elif not self.submitted and Order.objects.filter(client=self.client.id, submitted=False).exists():
            raise ValidationError('У пользователя уже есть незавершенный заказ! Нельзя добавить новый')

    def create(self):
        if not self.last_modified:
            self.last_modified = timezone.now()
        self.save()


class MessageType(models.Model):
    # TODO проблема с русской кодировкой. Если упею, разобраться
    body = models.TextField(null=True, blank=True)
    message_type = models.IntegerField(null=True, blank=True, unique=True)
    objects = models.Manager()

    def __str__(self):
        return str(self.body)


class Message(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True, blank=True)
    client = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, null=True, blank=True)
    message_type = models.ForeignKey(MessageType, on_delete=models.CASCADE, null=True, blank=True)
    viewed = models.BooleanField(default=False, null=False, blank=False)
    objects = models.Manager()

    def clean(self):
        # Проверка соответствия введенных данных типу сообщения
        # 1,2 - необходим только заказ
        # 3 - необходим только клиент
        if self.message_type.message_type == 3:
            if self.order:
                raise ValidationError('Нельзя указывать заказ для типа сообщения 3')
        elif self.message_type.message_type in [1, 2]:
            if self.client:
                raise ValidationError('Для типов 1,2 нельзя указывать клиента')

    def create(self):
        # Дополним клиента из заказа, если тип сообщения 1, 2
        if not self.client:
            self.client = self.order.client
        self.save()
        send_email.delay(self.id)

    def was_viewed(self):
        # Отметим сообщение прочитанным
        self.viewed = True
        self.save()

    def __str__(self):
        body = self.message_type.body[0:15] if len(self.message_type.body) < 16 else self.message_type.body[
                                                                                     0:15] + '...'
        message = 'Type: ' + str(self.message_type.message_type) + ' ' + body
        if self.client:
            message = message + ' Client: ' + str(self.client.username)
        if self.order:
            message = message + ' Order: ' + str(self.order.id)
        return message

    def get_detail_url(self):
        return reverse('read-message', args=(self.pk,))


class Statistics(models.Model):
    message_type = models.OneToOneField(MessageType, on_delete=models.CASCADE)
    total_count = models.IntegerField(default=0)
    readed_count = models.IntegerField(default=0)
    objects = models.Manager()

    @staticmethod
    def clear_statistic():
        Statistics.objects.all().delete()
