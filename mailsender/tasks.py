# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from shop.celery import app
from django.db import transaction, models
from django.db.models import Max, Q, OuterRef, Subquery
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.core.mail import send_mail
from django.template import Template, Context
import datetime


@app.task
def create_message_for_new_commit_order(order_id):
    from .models import Order, MessageType, Message

    try:
        order = Order.objects.get(id=order_id)
    except Order.DoesNotExist:
        return
    message_type = MessageType.objects.filter(message_type=1).first()
    if not message_type:
        return
    message = Message(message_type=message_type, order=order)
    message.create()
    implement_counter(1, 'total')


@app.task
def implement_counter(message_type, count_type):
    from .models import Statistics, MessageType

    message_type = MessageType.objects.filter(message_type=message_type).first()
    if not message_type:
        return
    with transaction.atomic():
        statistics = Statistics.objects.select_for_update().filter(message_type=message_type).first()
        if not statistics:
            statistics = Statistics(message_type=message_type)
        if count_type == 'total':
            statistics.total_count += 1
        elif count_type == 'readed':
            statistics.readed_count += 1

        statistics.save()


@app.task
def create_message():
    from .models import MessageType, Message, Order

    # Письма по пустой корзине
    orders = Order.objects.filter(submitted=False, last_modified__lte=timezone.now() - datetime.timedelta(days=7)).all()
    for order in orders:
        try:
            message = Message(message_type=MessageType.objects.get(message_type=2), order=order)
        except MessageType.DoesNotExist:
            break
        message.create()
        implement_counter(2, 'total')
    # Письма о долгом незаказе
    orders_q = Order.objects.values('client_id').filter(client_id=OuterRef('id')).annotate(
        last_modified_date=Max('last_modified')).filter(
        Q(last_modified_date__lte=timezone.now() - datetime.timedelta(days=90)) | Q(last_modified_date=None))
    users = get_user_model().objects.annotate(
        last_order_date=Subquery(orders_q.values('last_modified_date'), output_field=models.DateTimeField()))

    for user in users:
        try:
            message = Message(message_type=MessageType.objects.get(message_type=3),
                              client=user)
        except MessageType.DoesNotExist:
            return
        message.create()
        implement_counter(3, 'total')


@app.task
def send_email(message_id):
    from .models import Message

    REPORT_TEMPLATE = """
    Hello, {{ user.last_name }} {{ user.first_name }}
    We have a message for you:
        {{ message }}
    {% if order %}Order: {{ order }}{% endif %}
    """
    template = Template(REPORT_TEMPLATE)
    try:
        message = Message.objects.get(id=message_id)
    except Message.DoesNotExist:
        return
    send_mail(
        'Information from Shop',
        template.render(context=Context({'user': message.client, 'message': message.message_type.body,
                                         'order': message.order.id if message.order else None})),
        'from@shop.com',
        [message.client.email],
        fail_silently=False,
    )
