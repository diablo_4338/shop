# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from .models import Order, Message, Statistics
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from .tasks import implement_counter


def index(request):
    context = {'order': {'commited': True}}
    user = request.user
    if user.is_authenticated:
        # Информация об активном заказе
        if Order.objects.filter(client=user.id, submitted=False).exists():
            context['order']['commited'] = False
        # Информация о сообщениях
        messages = Message.objects.filter(client_id=user.id, viewed=False)
        context.update({'messages': messages})
        # Статистика отправленных сообщений
        if user.is_staff:
            statistics = {'statistics': Statistics.objects.all().order_by('message_type')}
            context.update(statistics)

    return render(request, 'mailsender/index.html', context)


@login_required(login_url='login')
def order(request):
    user = request.user
    if Order.objects.filter(client=user.id, submitted=False).exists():
        pass
    else:
        new_order = Order(client=user)
        new_order.create()
    return HttpResponseRedirect('/')


@login_required(login_url='login')
def commit_order(request):
    user = request.user
    Order.commit_order(user)

    return redirect('index')


@login_required(login_url='login')
def read_message(request, pk):
    user = request.user
    message = Message.objects.filter(id=pk).first()
    if message and user.id == message.client.id:
        message.was_viewed()
        implement_counter.delay(message.message_type.message_type, 'readed')
    return redirect('index')


@login_required(login_url='login')
def clear_statistic(request):
    if request.user.is_staff:
        Statistics.clear_statistic()
    return redirect('index')
