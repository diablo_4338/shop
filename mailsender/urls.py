# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import index, order, commit_order, read_message, clear_statistic

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^new-order/$', order, name='order'),
    url(r'^commit-order/$', commit_order, name='commit-order'),
    url(r'^read-message/(?P<pk>\d+)$', read_message, name='read-message'),
    url(r'^clear-statistics/$', clear_statistic, name='clear-statistics')
]
