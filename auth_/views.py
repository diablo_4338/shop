# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.views import LoginView, LogoutView
from django.http import HttpResponseRedirect


class CustomLogin(LoginView):
    template_name = 'auth_/login.html'
    next_page = 'index'


class CustomLogout(LogoutView):
    template_name = None
    next_page = 'index'


def login_(request):
    user = request.user
    if user.is_authenticated:
        return HttpResponseRedirect('/')
    return CustomLogin.as_view()(request)


def logout_(request):
    return CustomLogout.as_view()(request)
