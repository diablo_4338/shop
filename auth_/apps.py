# -*- coding: utf-8 -*-
from django.apps import AppConfig


class CustomAuthConfig(AppConfig):
    name = 'auth_'
