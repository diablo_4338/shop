# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^login/', views.login_, name='login'),
    url(r'^logout/', views.logout_, name='logout'),
   # url(r'', include('mailsender.urls', namespace='main'))
]
